import pyspark.sql.functions as f
from pyspark.sql import SparkSession
from pyspark.sql import Window
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DateType, TimestampType

spark = SparkSession.builder.appName('TMCI-Workbench-Example').master("local[*]").getOrCreate()

data = [

    # case 1 - person has two dqi_ids assigned (invalid).  We should error these records
    (1, "dod-1", "03/21/1992", "111-11-1111", 100, None, None),
    (2, "dod-1", "03/21/1993", "111-11-1111", 100, 'dqi-1', '2019-06-24 12:01:19.000'),
    (3, "dod-1", "03/21/1993", "111-11-1111", 100, 'dqi-0', '2019-06-24 12:01:19.000'),

    # case 2 - person has two birthdays, two ssns, and two new incremental records
    # we should not assign dqi_id to records with a birthday or ssn that is not part of the majority
    # we should assign a new dqi_id to any valid records that are not filtered out
    (4, "dod-2", "05/12/1982", "222-33-4444", 333, None, None),
    (5, "dod-2", "05/12/1982", "222-33-4444", 333, 'dqi-2', '2019-06-24 12:01:19.000'),
    (6, "dod-2", "05/11/1982", "222-33-4444", 333, 'dqi-2', '2019-06-24 12:01:19.000'),
    (7, "dod-2", "05/12/1982", "222-33-4440", 333, None, None),

    # case 3 - person has 1 new incremental record that should get the same dqi_id as the 
    # already assigned records (dqi-3)
    (8, "dod-3", "03/21/1993", "333-00-1111", 200, None, None),
    (9, "dod-3", "03/21/1993", "333-00-1111", 200, 'dqi-3', '2019-06-24 12:01:19.000'),
    (10, "dod-3", "03/21/1993", "333-00-1111", 200, 'dqi-3', '2019-06-24 12:01:19.000'),

    # case 4 - person is a new incremental record we have not seen before, we should assign dqi_id
    (11, "dod-4", "01/22/1988", "555-55-5555", 100, None, None),

    # case 5 - person is a new incremental set (more than 1 record), we should assign one new dqi_id
    # to both records.  they should both receive the same dqi_id
    (12, "dod-5", "05/10/1975", "777-77-7777", 300, None, None),
    (13, "dod-5", "05/10/1975", "777-77-7777", 300, None, None),

]

schema = StructType([
    StructField("master_record_id", IntegerType(), True),
    StructField("dodid", StringType(), True),
    StructField("dob", StringType(), True),
    StructField("patient_ssn", StringType(), True),
    StructField("site_id", IntegerType(), True),
    # CHANGE - we want to select dqi_id's that have been assigned already (if they exist)
    StructField("dqi_id", StringType(), True),
    # CHANGE - we want to bring in the last create date when the dqi_id was created
    StructField("dqi_created_at", StringType(), True),
])

df = spark.createDataFrame(data=data, schema=schema)
df.printSchema()
df.show(truncate=False)

# instead of creating a cluster_id, we want to operate within the context of a window partition 
# based on dodid as our primary clustering key.  then within a dodid cluster, we want to computer 
# various counts and percentages to determine the health of the individual rows in the cluster

window = Window.partitionBy("dodid").orderBy("dodid")

# total count of all records in a dodid cluster (group)
totalCount = f.count("*").over(window)

# calculate the total and distinct counts of dqi_ids within a dodid cluster (group)
dqiTotalCount = f.count("dqi_id").over(window)
dqiCount = f.size(f.collect_set("dqi_id").over(window))

# calculate the total ssns in a group, then the frequency of a given ssn value within the group, 
# then calculate the percentage
ssnCount = f.size(f.collect_set("patient_ssn").over(window))
ssnWindow = Window.partitionBy(["dodid", "patient_ssn"]).orderBy([
    "dodid", "patient_ssn"])
ssnFrequency = f.count("patient_ssn").over(ssnWindow)
ssnPercentage = ssnFrequency / totalCount

# calculate the total dobs in a group, then the frequency of a given dob value within the group, 
# then calculate the percentage
dobCount = f.size(f.collect_set("dob").over(window))
dobWindow = Window.partitionBy(["dodid", "dob"]).orderBy(["dodid", "dob"])
dobFrequency = f.count("dob").over(dobWindow)
dobPercentage = dobFrequency / totalCount

# we get the first dqi in a group, which will be the same for all since we only are working with groups 
# that have a distinct count of 1.  this value will be used to assign the group's dqi_id to other 
# matched records in the group that are new and have a null dqi_id
groupDqi = f.first("dqi_id", ignorenulls=True).over(window)

df = df \
    .filter("dob is NOT NULL AND patient_ssn is NOT NULL AND dodid is NOT NULL"
            # First, filter out any recrods with null dob/patient_ssn/dodid
            ) \
    .withColumn("total_count", totalCount) \
    .withColumn("dqi_id_count", dqiTotalCount) \
    .withColumn("dqi_id_distinct_count", dqiCount) \
    .withColumn("ssn_distinct_count", ssnCount) \
    .withColumn("ssn_frequency_count", ssnFrequency) \
    .withColumn("ssn_percentage", ssnPercentage) \
    .withColumn("dob_distinct_count", dobCount) \
    .withColumn("dob_frequency_count", dobFrequency) \
    .withColumn("dob_percentage", dobPercentage)


# create dataframe of all valid records with newly updated records having 1 or 2 for the dqi_update_type

valid_df = df \
    .filter('(ssn_percentage >= .75 and dob_percentage >= .75 and dqi_id_distinct_count <= 1) or (dqi_id is not null and dqi_id_distinct_count = 1)'
            # now we want to filter out records that don't meet our deterministic thresholds, or possible errors
            # so we remove any groups that have more than one distinct dqi_id suggesting we've made a prior mistake
            # and we select records that have a majority similarity for dob and ssn
    ) \
    .withColumn("group_dqi", groupDqi) \
    .withColumn("dqi_update_type",
                # here we are first looking to mark records that are matched in a group, but do not yet have a dqi_id
                # these would be records with a null dqi_id, but a group_dqi that is not null.  the group_dqi is simply
                # the first dqi_id found in a group, therefore a dqi_id exists for this set of matched recoreds
                # we will assign that dqi_id to the records in the group with a null dqi_id
                f.when(f.col("dqi_id").isNull() &
                       f.col("group_dqi").isNotNull(), 1)
                .otherwise(0)
                )\
    .withColumn("dqi_id",
                # here we are assigning the group dqi_id to the records that do not have one
                f.when(f.col("dqi_update_type") == 1, groupDqi).otherwise(f.col("dqi_id"))) \
    .withColumn("dqi_created_at",
                # here we want to assign timestamp to records that were just updated with the group_dqi (update type 1)
                f.when(f.col("dqi_update_type") == 1, f.current_timestamp())
                .otherwise(f.col("dqi_created_at"))
                )\
    .withColumn("dqi_update_type",
                # here we want to mark the remaining unassigned groups that are not part of an existing group, 
                # and do not have a dqi_id, these would be synonomous to new incremental records for people
                # we haven't seen before
                f.when(f.col("dqi_id_distinct_count") == 0, 2)
                .otherwise(f.col("dqi_update_type"))
                )\
    .withColumn("dqi_created_at",
                # here we want to assign timestamp to newly seen record groups that we are about to assign a 
                # new dqi_id (update type 2)
                f.when(f.col("dqi_update_type") == 2, f.current_timestamp())
                .otherwise(f.col("dqi_created_at"))
                )\
    .withColumn("dqi_id",
                # here we are assigning a new dqi_id to records for those people we are seeing for the first time
                f.when(f.col("dqi_update_type") == 2,
                       f.expr("first(uuid())").over(window))
                .otherwise(f.col("dqi_id"))
                )

# view dataframe valid dataframe
# We can register this as a table in Spark and then have another job operate against it and update the physical
# dqi table with all newly assigned or updated dqi records
valid_df \
    .select(["master_record_id", "dodid", "patient_ssn", "dob", "dqi_id", "dqi_created_at", \
    "dqi_update_type", "group_dqi", "ssn_percentage", "dob_percentage", "dqi_id_distinct_count"]) \
    .orderBy("master_record_id")\
    .show(truncate=False)


# create invalid dataframe with all records that were filtered out above, we can decide if this should
# should just be errors, then we can instead only filter where the dqi distinct count is > 1
invalid_df = df \
    .filter('!(ssn_percentage >= .75 and dob_percentage >= .75 and dqi_id_distinct_count <= 1) and !(dqi_id is not null and dqi_id_distinct_count = 1)') \
    .withColumn("error_group_id", f.expr("first(uuid())").over(window))

# view the invalid dataframe
invalid_df \
    .select(["master_record_id", "dodid", "patient_ssn", "dob", "dqi_id", "ssn_percentage", \
    "dob_percentage", "dqi_id_distinct_count", "error_group_id"]).orderBy("master_record_id")\
    .show(truncate=False)

